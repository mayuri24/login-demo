package com.example.logindemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.security.PublicKey;
import java.util.regex.Pattern;

public class LoginActivity extends AppCompatActivity {
    private EditText mEtUsername;
    private EditText mEtPassword;
    private TextView mTxtLogo;
    private TextView mTxtForgotPassword;
    private Button mBtnLogin;
    private Button mBtnSignIn;
    private ImageView ivShowHidePass;
    private boolean mPasswordFlag;


    boolean isValid;
    private static final Pattern PASSWORD_PATTERN =
            Pattern.compile("^" +
                    "(?=.*[0-9])" +         //at least 1 digit
                    "(?=.*[a-z])" +         //at least 1 lower case letter
                    "(?=.*[A-Z])" +         //at least 1 upper case letter
                    "(?=.*[a-zA-Z])" +      //any letter
                    "(?=.*[@#$%^&+=])" +    //at least 1 special character
                    "(?=\\S+$)" +           //no white spaces
                    ".{4,}" +               //at least 4 characters
                    "$");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        bindViews();

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateUsernamePassword();
            }
        });

    }
    private void bindViews() {

        mEtUsername = (EditText) findViewById(R.id.etUsername);
        mEtPassword = (EditText) findViewById(R.id.etPassword);
        mTxtLogo = (TextView) findViewById(R.id.txtLogo);
        mTxtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        mBtnLogin = (Button) findViewById(R.id.btnLogin);
        mBtnSignIn = (Button) findViewById(R.id.btnSignIn);
        ivShowHidePass= (ImageView) findViewById(R.id.ivShowHidePass);
        mPasswordFlag = false;


        ivShowHidePass.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (!mPasswordFlag) {
                            mEtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                            mEtPassword.setSelection(mEtPassword.getText().toString().length());
                            ivShowHidePass.setImageResource(R.drawable.icon_show_password);
                            mPasswordFlag = true;
                        } else {
                            mEtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                            mEtPassword.setSelection(mEtPassword.getText().toString().length());
                            ivShowHidePass.setImageResource(R.drawable.icon_password_hide);
                            mPasswordFlag = false;
                        }
                    }
                }
        );

    }

    public void validateUsernamePassword(){

            if (mEtUsername.getText().toString().isEmpty() && mEtPassword.getText().toString().isEmpty()) {
                mEtUsername.setError("This field cannot be empty");
                mEtPassword.setError("This field cannot be empty");
                isValid = false;
            } else if (!Patterns.EMAIL_ADDRESS.matcher(mEtUsername.getText().toString()).matches()) {
                mEtUsername.setError("Please Enter valid username");
                isValid = false;
            } else if (!PASSWORD_PATTERN.matcher(mEtPassword.getText().toString()).matches()) {
                mEtPassword.setError("Please Enter valid Password");
                isValid = false;
            }
            else {
                Toast.makeText(this, "Login Successfull", Toast.LENGTH_SHORT).show();
            }

    }


}
