package com.example.logindemo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class LoginHideunhideActivity extends AppCompatActivity {
    private TextView mTxt;
    private RelativeLayout mLay_PasswordContent;
    private EditText mEdtPassword;
    private ImageView mIvShowHidePass;
    private boolean mPasswordFlag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_hideunhide);
        bindview();
    }

    private void bindview() {
        mTxt = (TextView) findViewById(R.id.txt);
        mLay_PasswordContent = (RelativeLayout) findViewById(R.id.lay_PasswordContent);
        mEdtPassword = (EditText) findViewById(R.id.edtPassword);
        mIvShowHidePass = (ImageView) findViewById(R.id.ivShowHidePass);
        mPasswordFlag = false;


        mIvShowHidePass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Log.e("[LoginActivity]", "Inside setOnClickListener Of setOnClickListener");
                if (!mPasswordFlag) {
                    mEdtPassword.setTransformationMethod(HideReturnsTransformationMethod.getInstance());
                    mEdtPassword.setSelection(mEdtPassword.getText().toString().length());
                    mIvShowHidePass.setImageResource(R.drawable.icon_show_password);
                    mPasswordFlag = true;
                } else {
                    mEdtPassword.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    mEdtPassword.setSelection(mEdtPassword.getText().toString().length());
                    mIvShowHidePass.setImageResource(R.drawable.icon_password_hide);
                    mPasswordFlag = false;
                }
            }
        });

    }
}
